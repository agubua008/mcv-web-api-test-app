﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Entities;
using System;
using System.Threading.Tasks;
using System.Net.Mime;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class InsuranceController : ControllerBase
    {
        private IClientsService _clientsService;
        private IPoliciesService _policiesService;

        public InsuranceController(IClientsService clientsService, IPoliciesService policiesService)
        {
            _clientsService = clientsService;
            _policiesService = policiesService;
        }

        /// <summary>
        /// Get a Client by id.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /insurance/clientById/b2cbdea3-5bc6-4e14-8d21-579aba6845b2
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A client</returns>
        /// <response code="200">Returns the client</response>
        /// <response code="404">If the client is null</response>  
        [Authorize(Roles = Role.AdminUsers)]
        [ProducesResponseType(200, Type = typeof(Client))]
        [ProducesResponseType(404, Type = typeof(void))]
        [HttpGet("clientById/{id}")]
        public async Task<IActionResult> GetClientById(Guid id)
        {
            var client = await _clientsService.GetById(id);

            if (client == null)
            {
                return NotFound();
            }

            return Ok(client);
        }


        /// <summary>
        /// Get a Client by name.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /insurance/clientByName/dina
        ///
        /// </remarks>
        /// <param name="name"></param>
        /// <returns>A client</returns>
        /// <response code="200">Returns the client</response>
        /// <response code="404">If the client is null</response>  
        [Authorize(Roles = Role.AdminUsers)]
        [ProducesResponseType(200, Type = typeof(Client))]
        [ProducesResponseType(404, Type = typeof(void))]
        [HttpGet("clientByName/{name}")]
        public async Task<IActionResult> GetClientByName(string name)
        {
            var client = await _clientsService.GetByName(name);

            if (client == null)
            {
                return NotFound();
            }

            return Ok(client);
        }


        /// <summary>
        /// Get a list of policies by client name.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /insurance/policiesByClientByName/dina
        ///
        /// </remarks>
        /// <param name="name"></param>
        /// <returns>A list of policies</returns>
        /// <response code="200">Returns the list of policies on a client</response>
        /// <response code="404">If the client does not exist</response>  
        [Authorize(Roles = Role.Admin)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Policy>))]
        [ProducesResponseType(404, Type = typeof(void))]
        [HttpGet("policiesByClientByName/{name}")]
        public async Task<IActionResult> GetPoliciesByClientByName(string name)
        {
            var client = await _clientsService.GetByName(name);
            if (client == null)
            {
                return NotFound();
            }
            var policies = await _policiesService.GetListByClientId(client.id);
            if (policies == null)
            {
                return NotFound();
            }

            return Ok(policies);
        }


        /// <summary>
        /// Get a client by id policy.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /insurance/clientIdByPolicyId/64cceef9-3a01-49ae-a23b-3761b604800b
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A client</returns>
        /// <response code="200">Returns the client related to a id policy</response>
        /// <response code="404">If the id policy does not exist</response>  
        [Authorize(Roles = Role.Admin)]
        [ProducesResponseType(200, Type = typeof(Client))]
        [ProducesResponseType(404, Type = typeof(void))]
        [HttpGet("clientIdByPolicyId/{id}")]
        public async Task<IActionResult> GetClientByPolicyId(Guid id)
        {
            var idClient = await _policiesService.GetClientIdByPolicyId(id);
            if (idClient == null)
            {
                return NotFound();
            }
            var client = await _clientsService.GetById(idClient.Value);
            if (client == null)
            {
                return NotFound();
            }

            return Ok(client);
        }
    }
}
