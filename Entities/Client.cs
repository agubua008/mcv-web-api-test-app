using System;

namespace WebApi.Entities
{
    public class Client
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string role { get; set; }
    }
}