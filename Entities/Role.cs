namespace WebApi.Entities
{
    public static class Role
    {
        public const string Admin = "admin";
        public const string Users = "users";
        public const string AdminUsers = "admin,users";
    }
}