using System;

namespace WebApi.Entities
{
    public class Policy
    {
        public Guid id { get; set; }
        public double amountInsured { get; set; }
        public string email { get; set; }
        public DateTime inceptionDate { get; set; }
        public bool installmentPayment { get; set; }
        public Guid clientId { get; set; }
    }
}