# Mcv Web API Test App

ASP.NET Core 2.2 - MVC Web API

Bienvenidos a la prueba de backend de AXA.
Es proyecto tiene como objetivo demostrar las habilidades de programacion en mediante ASP.NET Core 2.2 - MVC Web API.


Requisitos previos:
    
*  Debe tener instalado ASP DotNetCore 2.2 en la PC. [ASP DotNetCore 2.2](https://dotnet.microsoft.com/download/dotnet-core/2.2)
*  Debe tener instalado git en la PC. [git](https://www.git-scm.com/)

Pasos para ejecutar el proyecto:
*  Ir a la carpeta del proyecto y abrir el proyecto mediante Visual Studio 2019
*  Hacer click sobre el boton 'play' IIS Express, el proyecto levantara sobre 'http://localhost:57645/'
*  Esperar que el browser por defecto de ejecute o ingresar a su navegador favorito e ingresar a [http://localhost:57645](http://localhost:57645)


Una ves all�, proda visualizar el Swagger del micro servicio, junto con la documentacion de cada uno de los recursos de la API.

El primer paso es solicitar una token para alguno de los dos tipos de roles que posee la aplicaci�n  "admin" o "users", los usuarios para probar cada uno de estos roles
son:

Rol **admin** => Usuario: **admin**, Password: **admin**
Rol **users** => Usuario: **user**, Password: **user**

Ejemplo curl para solicitar Token:

REQUEST

curl -X POST \
  http://localhost:57645/users/authenticate \
  -H 'Content-Type: application/json' \
  -d '{
	"Username":"user",
	"Password":"user"
}'

RESPONSE

{
    "id": 2,
    "firstName": "Normal",
    "lastName": "User",
    "username": "user",
    "password": null,
    "role": "users",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjIiLCJyb2xlIjoidXNlcnMiLCJuYmYiOjE1NzExMTUyNzcsImV4cCI6MTU3MTcyMDA3NywiaWF0IjoxNTcxMTE1Mjc3fQ.qZ6guHqH24iACkI5WLKLjWfuIeNeh9Li7dHW3hEsWAE"
}


Una ves que ya posee una Token puede llamar a cualquiera de los 4 Recursos que posee la API, ellos son:

Recuerde que debe enviar la Header **Authorization** con el valor **Bearer {Token}** para cualquiera de los 4 Recursos.


*  GET /insurance/clientById/{id} (Obtiene un cliente mediante su id) **Roles admin-users**

*  GET /insurance/clientByName/{name} (Obtiene un cliente mediante su nombre) **Roles admin-users**

*  GET /insurance/policiesByClientByName/{name} (Obtiene una lista de politicas mediante el nombre del cliente asociado a ellas) **Rol admin**

*  GET /insurance/clientIdByPolicyId/{id} (Obtiene un cliente mediante el id de alguna de sus politicas asociadas) **Rol admin**