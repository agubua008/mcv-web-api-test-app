
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IClientsService
    {
        Task<Client> GetById(Guid id);
        Task<Client> GetByName(string name);
    }

    public class ClientsService : IClientsService
    {
        private const string BASE_URL = "http://www.mocky.io/v2/";
        private const string RESOURCE = "5808862710000087232b75ac";
        private HttpClient _client;

        public ClientsService()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(BASE_URL);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<Client> GetById(Guid id)
        {
            Client client = null;
            var response = await _client.GetAsync(RESOURCE);
            if (response.IsSuccessStatusCode)
            {
                var d = await response.Content.ReadAsAsync<JObject>();
                var clients = d["clients"].ToObject<IEnumerable<Client>>();
                client = clients.FirstOrDefault(p => p.id == id);
            }
            return client;
        }

        public async Task<Client> GetByName(string name)
        {
            Client client = null;
            if (string.IsNullOrEmpty(name))
            {
                return client;
            }
            var response = await _client.GetAsync(RESOURCE);
            if (response.IsSuccessStatusCode)
            {
                var d = await response.Content.ReadAsAsync<JObject>();
                var clients = d["clients"].ToObject<IEnumerable<Client>>();
                client = clients.FirstOrDefault(p => !string.IsNullOrEmpty(p.name) && p.name.ToLower() == name.ToLower());
            }
            return client;
        }
    }
}