
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IPoliciesService
    {
        Task<IEnumerable<Policy>> GetListByClientId(Guid idClient);
        Task<Guid?> GetClientIdByPolicyId(Guid idPolicy);
    }

    public class PoliciesService : IPoliciesService
    {
        private const string BASE_URL = "http://www.mocky.io/v2/";
        private const string RESOURCE = "580891a4100000e8242b75c5";
        private HttpClient _client;

        public PoliciesService()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(BASE_URL);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<IEnumerable<Policy>> GetListByClientId(Guid idClient)
        {
            IEnumerable<Policy> policies = null;
            var response = await _client.GetAsync(RESOURCE);
            if (response.IsSuccessStatusCode)
            {
                var d = await response.Content.ReadAsAsync<JObject>();
                var pols = d["policies"].ToObject<IEnumerable<Policy>>();
                policies = pols.Where(p => p.clientId == idClient);
            }
            return policies;
        }


        public async Task<Guid?> GetClientIdByPolicyId(Guid idPolicy)
        {
            Guid? guid = null;
            var response = await _client.GetAsync(RESOURCE);
            if (response.IsSuccessStatusCode)
            {
                var d = await response.Content.ReadAsAsync<JObject>();
                var pols = d["policies"].ToObject<IEnumerable<Policy>>();
                guid = pols.FirstOrDefault(p => p.id == idPolicy)?.clientId;
            }
            return guid;
        }

    }
}